﻿using System;
using System.Linq;
using Microsoft.SharePoint.Client;
using System.Web.Configuration;
using MT.Collaboration_ForkWeb.Common;

namespace MT.Collaboration_ForkWeb.Helpers
{
    public class BrandingHelpers
    {
        private string _hostWebUrl = "";
        private Web _web;
        private ClientContext _ctx;
        public BrandingHelpers(ClientContext ctx)
        {
            _web = ctx.Web;
            _ctx = ctx;

        }
        public void ApplySiteBranding()
        {
            RemoveAppFromRecent();
            ApplyCustomActionToScriptLink("JS", "mt-branding.js");
            ApplyCustomActionToScriptLink("CSS", "sharepoint.search.css");
            AddHideNavigationActionToSiteSettings("Hide User-Interface Elements");
            _web.SetPropertyBagValue("NewBrandigSetUp", "true");
        }
        public void RemoveSiteBranding()
        {
            RemoveCustomaActionFromScriptLink("MT_JS_Link");
            RemoveCustomaActionFromScriptLink("MT_CSS_Link");
            RemoveCustomaActionFromSettings("Hide User-Interface Elements");
            _web.SetPropertyBagValue("NewBrandigSetUp", "false");
        }
        private void ApplyCustomActionToScriptLink(string fileType, string fileName)
        {

            _hostWebUrl = Utility.GetWCFUrl();
            _hostWebUrl = _hostWebUrl.Substring(0, _hostWebUrl.IndexOf("Services") - 1) + "/";

            string scriptBlock = "";
            string url = _hostWebUrl + fileType + "/" + fileName;
            string revision = Guid.NewGuid().ToString().Replace("-", "");
            string fileLink = string.Format("{0}?rev={1}", url, revision);

            string actionName = "MT_" + fileType + "_Link";
            switch (fileType)
            {
                case "JS":
                    scriptBlock = @"document.write('\x3Cscript type=""text/javascript"" src=""" + fileLink + @""">\x3C/script>');";
                    break;
                case "CSS":
                    scriptBlock = @"document.write('<link rel=""stylesheet"" href=""" + fileLink + @"""/>');";
                    break;

            }


            CleanExistingCustomActionsByName(actionName);
            UserCustomAction jsAction = _web.UserCustomActions.Add();
            jsAction.Location = "ScriptLink";
            jsAction.ScriptBlock = scriptBlock;
            jsAction.Name = actionName;

            jsAction.Update();
            _ctx.ExecuteQuery();

        }
        private void RemoveCustomaActionFromScriptLink(string actionName)
        {
            CleanExistingCustomActionsByName(actionName);

        }
        private void RemoveCustomaActionFromSettings(string actionTitle)
        {
            CleanExistingCustomActionsByTitle(actionTitle);

        }
        private void RemoveAppFromRecent()
        {
            // Remove the entry from the 'Recents' node
            NavigationNodeCollection nodes = _web.Navigation.QuickLaunch;
            _ctx.Load(nodes, n => n.IncludeWithDefaultProperties(c => c.Children));
            _ctx.ExecuteQuery();
            var recent = nodes.Where(x => x.Title == "Recent").FirstOrDefault();
            if (recent != null)
            {
                var appLink = recent.Children.Where(x => x.Title == Const.AddInTitle).FirstOrDefault();
                if (appLink != null) appLink.DeleteObject();
                _ctx.ExecuteQuery();
            }
        }
        private void AddHideNavigationActionToSiteSettings(string actionName)
        {

            _ctx.Load(_web, w => w.UserCustomActions);
            _ctx.ExecuteQuery();

            _ctx.Load(_web, w => w.UserCustomActions, w => w.Url, w => w.AppInstanceId);
            _ctx.ExecuteQuery();

            UserCustomAction userCustomAction = _web.UserCustomActions.Add();
            userCustomAction.Location = "Microsoft.SharePoint.SiteSettings";
            userCustomAction.Group = "Customization";
            BasePermissions perms = new BasePermissions();
            perms.Set(PermissionKind.ManageWeb);
            userCustomAction.Rights = perms;
            userCustomAction.Sequence = 100;
            userCustomAction.Title = actionName;

            string realm = TokenHelper.GetRealmFromTargetUrl(new Uri(_ctx.Url));
            string issuerId = WebConfigurationManager.AppSettings.Get("ClientId");


            var modifyPageUrl = string.Format("{0}Pages/ManageNavigation.aspx?{{StandardTokens}}", _hostWebUrl);
            string url = "javascript:LaunchApp('{0}', 'i:0i.t|ms.sp.ext|{1}@{2}','{3}',{{width:600,height:400,title:'Modify Site'}});";
            url = string.Format(url, Guid.NewGuid().ToString(), issuerId, realm, modifyPageUrl);

            userCustomAction.Url = url;
            userCustomAction.Update();
            _ctx.ExecuteQuery();


        }
        private void CleanExistingCustomActionsByName(string actionName)
        {
            // Clean up existing actions that we may have deployed
            var existingActions = _web.UserCustomActions;
            _ctx.Load(existingActions);
            _ctx.ExecuteQuery();
            var actions = existingActions.ToArray();
            foreach (var existingAction in actions)
            {
                if (existingAction.Name.Equals(actionName, StringComparison.InvariantCultureIgnoreCase))
                    existingAction.DeleteObject();
            }
            _ctx.ExecuteQuery();
        }
        private void CleanExistingCustomActionsByTitle(string actionTitle)
        {
            _ctx.Load(_web, w => w.UserCustomActions);
            _ctx.ExecuteQuery();

            foreach (var action in _web.UserCustomActions)
            {
                if (action.Title == actionTitle)
                {
                    action.DeleteObject();
                    _ctx.ExecuteQuery();
                    break;
                }
            }
        }
    }
}