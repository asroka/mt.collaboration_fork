﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace MT.Collaboration_ForkWeb.Helpers
{
    public class Utility
    {
        //Get WCF URL where this message was handled
        public static string GetWCFUrl()
        {

            OperationContext op = OperationContext.Current;
            Message msg = op.RequestContext.RequestMessage;
            return msg.Headers.To.ToString();
        }
    }
}