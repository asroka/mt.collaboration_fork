﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.EventReceivers;
using MT.Collaboration_ForkWeb.Common;
using MT.Collaboration_ForkWeb.Management;
using MT.Collaboration_ForkWeb.Helpers;

namespace MT.Collaboration_ForkWeb.Services
{
    public class AppEventReceiver : IRemoteEventService
    {
        public const string RECEIVER_NAME =Const.AppInstallCollabForkEventReciverName;
        /// <summary>
        /// Handles app events that occur after the app is installed or upgraded, or when app is being uninstalled.
        /// </summary>
        /// <param name="properties">Holds information about the app event.</param>
        /// <returns>Holds information returned from the app event.</returns>
        public SPRemoteEventResult ProcessEvent(SPRemoteEventProperties properties)
        {
            SPRemoteEventResult result = new SPRemoteEventResult();
            switch (properties.EventType)
            {
                case SPRemoteEventType.AppInstalled:
                    ApplySiteBranding(properties);
                    break;
                case SPRemoteEventType.AppUninstalling:
                    RemoveSiteBranding(properties);
                    break;
                case SPRemoteEventType.WebProvisioned:
                    ApplySubSiteBranding(properties);
                    break;
            }

            return result;
        }

        /// <summary>
        /// This method is a required placeholder, but is not used by app events.
        /// </summary>
        /// <param name="properties">Unused.</param>
        public void ProcessOneWayEvent(SPRemoteEventProperties properties)
        {
            //throw new NotImplementedException();
        }

        private void ApplySiteBranding(SPRemoteEventProperties properties)
        {
            using (ClientContext ctx = TokenHelper.CreateAppEventClientContext(properties, false))
            {
                if (ctx != null)
                {
                    if (ctx.Web.GetPropertyBagValueString("NewBrandigSetUp", "false") != "true")
                    {
                        Web web = ctx.Web;
                        Site site = ctx.Site;
                        new RemoteEventReceiverManager().AttachWebProvisionRemoteReciver(ctx);
                        new BrandingHelpers(ctx).ApplySiteBranding();


                    }
                }
            }
        }
        private void ApplySubSiteBranding(SPRemoteEventProperties properties)
        {
            using (ClientContext ctx = TokenHelper.CreateRemoteEventReceiverClientContext(properties))
            {
                if (ctx != null)
                {
                    if (ctx.Web.GetPropertyBagValueString("NewBrandigSetUp", "false") != "true")
                    {
                        new RemoteEventReceiverManager().AttachWebProvisionRemoteReciver(ctx);
                        new BrandingHelpers(ctx).ApplySiteBranding();


                    }
                }
            }
        }


        private void RemoveSiteBranding(SPRemoteEventProperties properties)
        {
            using (ClientContext ctx = TokenHelper.CreateAppEventClientContext(properties, false))
            {
                if (ctx != null)
                {
                    new BrandingHelpers(ctx).RemoveSiteBranding();
                    new RemoteEventReceiverManager().RemoveWebProvisionedEventReceiver(ctx);
                }
            }
        }

    }
}

