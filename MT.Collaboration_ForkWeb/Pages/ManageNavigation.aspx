﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageNavigation.aspx.cs" Inherits="MT.Collaboration_ForkWeb.Pages.ManageNavigation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="margin-top: 20px;text-align:center;font-family:'Segoe UI','Segoe',Tahoma,Helvetica,Arial,sans-serif;">
                <fieldset id="ListsFieldSet" runat="server">
                    <legend>
                        <asp:Label ID="lblLegend" runat="server" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_hidenavpane_title%>"></asp:Label></legend>
                    <div>
                        <asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_hidenavpane_description%>"></asp:Label>
                        <asp:DropDownList ID="ddlNavigationOptions" runat="server">
                            <asp:ListItem Value="No" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_hidenavpane_hidenot%>" />
                            <asp:ListItem Value="All" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_hidenavpane_hideall%>" />
                            <asp:ListItem Value="Start" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_hidenavpane_hidestart%>" />
                        </asp:DropDownList>
                    </div>
                </fieldset>
            </div>
        <div style="margin-top: 20px;text-align:center">
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_savebutton_text%>" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:MT.Branding.Resources, lbl_hideuielements_cancelbutton_text%>" OnClick="btnCancel_Click" />
        </div>
        </div>
    </form>
</body>
</html>
