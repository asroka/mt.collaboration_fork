﻿using System;
using Microsoft.SharePoint.Client;
using MT.Collaboration_ForkWeb.Helpers;
using MT.Collaboration_ForkWeb.Management;

namespace MT.Collaboration_ForkWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Uri redirectUrl;
            switch (SharePointContextProvider.CheckRedirectionStatus(Context, out redirectUrl))
            {
                case RedirectionStatus.Ok:
                    return;
                case RedirectionStatus.ShouldRedirect:
                    Response.Redirect(redirectUrl.AbsoluteUri, endResponse: true);
                    break;
                case RedirectionStatus.CanNotRedirect:
                    Response.Write("An error occurred while processing your request.");
                    Response.End();
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string hostWeb = Request.QueryString["SPHostUrl"];
            link.NavigateUrl = hostWeb;
        }

        protected void btnInstall_Click(object sender, EventArgs e)
        {
            var spContext = SharePointContextProvider.Current.GetSharePointContext(Context);

            using (var ctx = spContext.CreateUserClientContextForSPHost())
            {
                if (ctx != null)
                {
                    new BrandingHelpers(ctx).ApplySiteBranding();
                    new RemoteEventReceiverManager().AttachWebProvisionRemoteReciver(ctx);
                }
            }
        }

        protected void btnUnistall_Click(object sender, EventArgs e)
        {
            var spContext = SharePointContextProvider.Current.GetSharePointContext(Context);

            using (var ctx = spContext.CreateUserClientContextForSPHost())
            {
                if (ctx.Web.GetPropertyBagValueString("NewBrandigSetUp", "false") != "true")
                {
                    new BrandingHelpers(ctx).RemoveSiteBranding();
                    new RemoteEventReceiverManager().RemoveWebProvisionedEventReceiver(ctx);
                }

            }
        }
    }
}