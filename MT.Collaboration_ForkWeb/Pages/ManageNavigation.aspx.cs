﻿using Microsoft.SharePoint.Client;
using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;

namespace MT.Collaboration_ForkWeb.Pages
{
    public partial class ManageNavigation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                var spContext = SharePointContextProvider.Current.GetSharePointContext(Context);

                using (var ctx = spContext.CreateUserClientContextForSPHost())
                {
                    if (ctx != null)
                    {

                        Web hostWeb = ctx.Web;
                        ctx.Load(hostWeb);
                        ctx.ExecuteQuery();
                        ddlNavigationOptions.SelectedIndex = ddlNavigationOptions.Items.IndexOf(ddlNavigationOptions.Items.FindByValue(hostWeb.GetPropertyBagValueString("NavigationVisibility", "")));

                    }

                }





            }

        }
        protected override void InitializeCulture()
        {
            if (Request.QueryString["SPLanguage"] != null)
            {
                string selectedLanguage = Request.QueryString["SPLanguage"];

                // Override the page language.
                UICulture = selectedLanguage;
                Culture = selectedLanguage;

                // Reset the thread language.
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new
                    CultureInfo(selectedLanguage);
            }
            base.InitializeCulture();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var spContext = SharePointContextProvider.Current.GetSharePointContext(Context);

            using (var ctx = spContext.CreateUserClientContextForSPHost())
            {
                if (ctx != null)
                {
                    Web hostWeb = ctx.Web;
                    hostWeb.SetPropertyBagValue("NavigationVisibility", ddlNavigationOptions.SelectedValue);
                }
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateMsg", "window.parent.postMessage('CloseCustomActionDialogRefresh', '*');", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateMsg", "window.parent.postMessage('CloseCustomActionDialogRefresh', '*');", true);
        }
    }
}