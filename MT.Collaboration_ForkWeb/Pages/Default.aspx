﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MT.Collaboration_ForkWeb.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>
                <asp:HyperLink ID="link" runat="server">New branding applied to host web</asp:HyperLink>
            </p>
            <p>
                <asp:Button ID="btnUnistall" runat="server" Text="Uninstall" OnClick="btnUnistall_Click"/>
                <asp:Button ID="btnInstall" runat="server" Text="Install" OnClick="btnInstall_Click"/>
            </p>
        </div>
    </form>
</body>
</html>