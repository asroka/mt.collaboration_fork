//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources.MT.Branding {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.MT.Branding.Resources", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string lbl_hideuielements_cancelbutton_text {
            get {
                return ResourceManager.GetString("lbl_hideuielements_cancelbutton_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use this section to hide the entire left (quick-launch) navigation pane..
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_description {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hide on whole site.
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_hideall {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_hideall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Don&apos;t hide.
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_hidenot {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_hidenot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hide only on start page.
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_hidestart {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_hidestart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Display &quot;Show All Site Content&quot; in the &quot;Site Actions&quot; menu..
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_showallsitecontent {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_showallsitecontent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Navigation Pane.
        /// </summary>
        internal static string lbl_hideuielements_hidenavpane_title {
            get {
                return ResourceManager.GetString("lbl_hideuielements_hidenavpane_title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use this page to hide standard user-interface elements..
        /// </summary>
        internal static string lbl_hideuielements_page_description {
            get {
                return ResourceManager.GetString("lbl_hideuielements_page_description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hide User-Interface Elements.
        /// </summary>
        internal static string lbl_hideuielements_page_title {
            get {
                return ResourceManager.GetString("lbl_hideuielements_page_title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string lbl_hideuielements_savebutton_text {
            get {
                return ResourceManager.GetString("lbl_hideuielements_savebutton_text", resourceCulture);
            }
        }
    }
}
