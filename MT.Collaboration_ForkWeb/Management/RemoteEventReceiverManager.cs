﻿
using System.Linq;
using Microsoft.SharePoint.Client;
using MT.Collaboration_ForkWeb.Helpers;
using MT.Collaboration_ForkWeb.Common;

namespace MT.Collaboration_ForkWeb.Management
{
    public class RemoteEventReceiverManager
    {
        const string WEB_PROVISIONED_RECIVER_NAME = Const.WebProvisionedReciverName;

        public void AttachWebProvisionRemoteReciver(ClientContext ctx)
        {
            EventReceiverDefinitionCreationInformation receiver =
                new EventReceiverDefinitionCreationInformation();
            receiver.EventType = EventReceiverType.WebProvisioned;


            receiver.ReceiverUrl = Utility.GetWCFUrl();

            receiver.ReceiverName = WEB_PROVISIONED_RECIVER_NAME;
            receiver.Synchronization = EventReceiverSynchronization.Synchronous;

            //Add the new event receiver to site context
            ctx.Site.EventReceivers.Add(receiver);
            ctx.ExecuteQuery();

        }
        public void RemoveWebProvisionedEventReceiver(ClientContext clientContext)
        {
            var recievers = clientContext.Web.EventReceivers.FirstOrDefault(i => i.ReceiverName == WEB_PROVISIONED_RECIVER_NAME);
            if (recievers != null)
            {
                recievers.DeleteObject();
                clientContext.ExecuteQuery();
            }
        }
    }
}