﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MT.Collaboration_ForkWeb.Common
{
    public class Const
    {
        public const string WebProvisionedReciverName = "WebProvisionedEventReciver";
        public const string AppInstallCollabForkEventReciverName = "MTCollaboration_Fork";
        public const string AddInTitle = "MT.Collaboration_Fork";
    }
}