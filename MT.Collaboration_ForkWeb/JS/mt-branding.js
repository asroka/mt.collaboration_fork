﻿var jQuery = "https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.2.min.js";
var properties;
var propertyValues;
var selectedPropertyValue;

// Is MDS enabled?
if ("undefined" !== typeof g_MinimalDownload && g_MinimalDownload && (window.location.pathname.toLowerCase()).endsWith("/_layouts/15/start.aspx") && "undefined" !== typeof asyncDeltaManager) {
    // Register script for MDS if possible
    RegisterModuleInit("mt-branding.js", JavaScript_Embed); //MDS registration
    JavaScript_Embed(); //non MDS run
} else {
    JavaScript_Embed();
}

function JavaScript_Embed() {

    loadScript(jQuery, function () {
        $(document).ready(function () {
            // Execute status setter only after SP.JS has been loaded
            SP.SOD.executeOrDelayUntilScriptLoaded(function () { SetLayout(); }, 'sp.js');
        });
    });
}

function SetLayout()
{
    SetFooter();
    SetNavigationStyling();
}

function IsOnPage(pageName) {
    if (window.location.href.toLowerCase().indexOf(pageName.toLowerCase()) > -1) {
        return true;
    } else {
        return false;
    }
}
function SetNavigationStyling() {
    var promiseWepPropertiesUploaded = getAllWebProperty();
    promiseWepPropertiesUploaded.then(function (exist) {
        console.log("property: "+getWebProperty("NavigationVisibility"));
        switch(getWebProperty("NavigationVisibility")) {
            case "Start":
                if (IsOnPage("Home.aspx"))
                {
                    HideLeftNavigation();
                }
                break;
            case "All":
                HideLeftNavigation();
                break;
        }
    });    
}
function HideLeftNavigation()
{
    $("#sideNavBox").hide();
    $("#contentBox").css("margin-left", "50px");

}
function SetFooter()
{
    var headID = document.getElementById("s4-workspace");
    var FooterNode = document.createElement('div');
    FooterNode.className = "mt-footer ms-dialogHidden"
    if (document.getElementsByClassName('mt-footer').length === 0) {
        var FooterBody = '<div class="mt-footer ms-dialogHidden"><div class="mt-footer-left">Owner:<a href="#">Wehrli Holzer Manuel MTII-GF</a></div><div class="mt-footer-right">Site contact: <a href="#">Konczak Aleksandra MT-PL</a> </div></div>';
        FooterNode.innerHTML = FooterBody;
        headID.appendChild(FooterNode);
    }


}

function loadScript(url, callback) {
    var head = document.getElementsByTagName("head")[0];
    var script = document.createElement("script");
    script.src = url;

    // Attach handlers for all browsers
    var done = false;
    script.onload = script.onreadystatechange = function () {
        if (!done && (!this.readyState
					|| this.readyState === "loaded"
					|| this.readyState === "complete")) {
            done = true;

            // Continue your code
            callback();

            // Handle memory leak in IE
            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        }
    };

    head.appendChild(script);
}


function getAllWebProperty() {
    var deferred = $.Deferred();
    var ctx = new SP.ClientContext.get_current();
    var web = ctx.get_web();
    properties = web.get_allProperties();
    ctx.load(properties);
    ctx.executeQueryAsync(successHandler, errorHandler);
    function successHandler(sender, args) {
        console.log("succes " + properties);
        deferred.resolve(true);
    }
    function errorHandler(sender, args) {
        deferred.resolve(false);
    }

    return deferred.promise();
}

function getWebProperty(propertyKey) {

    propertyValues = properties.get_fieldValues();
    return propertyValues[propertyKey];

}

